<?php

namespace App\Http\Controllers;

use App\Models\PostComment;
use App\Models\PostLike;
use Illuminate\Http\Request;
// access the authenticated user via Auth Facade 
use Illuminate\Support\Facades\Auth;
use App\Models\Post;

class PostController extends Controller
{
    public function create()
    {
        return view('posts.create');
    }

    public function store(Request $request) 
    {
        if(Auth::user()){
            //instantiate a new Post object from the Post model
            $post = new Post();
            // define the porperties of the $post object using the received form data
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            // get the id of the authenticated user and set it as the foreign key for the user_id of the new post
            $post->user_id = Auth::user()->id;
            // save this $post object to the database
            $post->save();
            return redirect('/posts');
        } else {
            return redirect('/login');
        }
    }

    // action that will return a view showing all the blog posts
    public function index()
    {
        $posts = Post::where('isActive', true)->get();
        return view('posts.index')->with('posts', $posts);
    }
    
    public function welcome()
    {
        $posts = Post::where('isActive', true)->inRandomOrder()->limit(3)->get();
        return view('welcome')->with('posts', $posts);
    }

    // action for showing only the posts authored by the user
    public function myPosts()
    {
        if(Auth::user()){
            $posts = Auth::user()->posts;
            return view('posts.index')->with('posts', $posts);
        } else  {
            return redirect('/login');
        }
    }

    // action for showing only the specific post using the url parameter $id to query for the database entry to be shown
    public function show($id)
    {
        $post = Post::find($id);
        return view('posts.show')->with('post', $post);

    }

    public function edit($id)
    {
        $post = Post::find($id);
        if(Auth::user()){
            if($post->user_id == Auth::user()->id) {
                return view('posts.edit')->with('post', $post);
            }
            return redirect('/posts');

        } else {
            return redirect('/login');
        }
    }

    public function update(Request $request, $id)
    {
        $post = Post::find($id);
        if(Auth::user()->id == $post->user_id) {
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            $post->save();
        } 
        return redirect('/posts');
    }

/*     public function destroy($id)
    {
        $post = Post::find($id);
        if(Auth::user()->id == $post->user_id) {
            $post->delete();
        }
        return redirect('/posts');
    } */

    public function archive($id)
    {
        $post = Post::find($id);
        if(Auth::user()->id == $post->user_id) {
            $post->isActive = false;
            $post->save();
        }
        return redirect('/posts');
    }

    public function like($id)
    {
        $post = Post::find($id);
        $user_id = Auth::user()->id;

        // if authenticated user is not hte post author
        if($post->user_id != $user_id) {
            // checks if a post like has been made by the logged in user before
            if($post->likes->contains('user_id', $user_id)) {
                //deletes the like made by the user to unlike the post
                PostLike::where('post_id', $post->id)->where('user_id', $user_id)->delete();
            } else {
                // to instantiate a new PostLike object from the postlike model
                $postLike = new PostLike;
                $postLike->post_id = $post->id;
                $postLike->user_id = $user_id;
                $postLike->save();
            }
            return redirect("/posts/$id");
        }  
    }

    public function comment(Request $request, $id)
    {
        $post = Post::find($id);
        $user_id = Auth::user()->id;
        if(Auth::user()){
            $postComment = new PostComment;
            $postComment->post_id = $post->id;
            $postComment->user_id = $user_id;
            $postComment->content = $request->input('content');
            $postComment->save();

            return redirect("/posts/$id");
        } else {
            return redirect('/login');
        }
        
            
    }

}
