@extends('layouts.app')

@section('content')'
    <div class="card">
        <div class="card-body">
            <h2 class="card-title">{{$post->title}}</h2>
            <p class="card-subtitle text-muted" >Author: {{$post->user->name}} </p>
            <p class="card-subtitle text-muted mb-3" >Created At: {{$post->created_at}} </p>
            <p class="card-text" >{{$post->content}} </p>

            @if (Auth::id() != $post->user_id)
                <form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">
                    @method('PUT')
                    @csrf

                    @if ($post->likes->contains("user_id", Auth::id()))
                        <button type="submit" class="btn btn-danger">Unlike</button>
                    @else
                        <button type="submit" class="btn btn-success">Like</button>
                    @endif
                    
            @endif
                </form>
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                        Add comment
                    </button>
                    
                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Add comment</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <form method="POST" action="/posts/{{$post->id}}/comment">
                    
                                @csrf
                            <div class="modal-body">

                                    <textarea type="text" name="content" id="content" cols="55" rows="5" placeholder="Write comment here.." required></textarea>
                               
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success" >Comment</button>
                            </div>
                            </form>
                            </div>
                        </div>
                        </div>
                    </div>



            <div class="mt-3" >
                <a href="/posts" class="card-link">View all posts</a>
            </div>
        </div>

        <div class="mt-3" >
            <h2> Comments </h2>
        
            @foreach ($post->comments as $comment)
                @if($post->id == $comment->post_id)
                    <div class="card mb-3" >
                        <div class="card-body">
                            <h4>{{$comment->user->name}}: {{$comment->content}}</h4>
                            <p>{{$comment->created_at}}</p>
                        </div>
                    </div>
                @endif
            @endforeach
    
        </div>

    </div>



@endsection