@extends('layouts.app')

@section('content')
    <div class="mx-5">
        <form method="POST" action="/posts" >
            @csrf
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" name="title" id="title" class="form-control">
            </div>
            <div class="form-group">
                <label for="content">Content</label>
                <textarea type="text" name="content" id="content" class="form-control" rows="3"></textarea>
            </div>
            <div class="mt-2">
            <button type="submit" class="btn btn-primary">Create Post</button>
            </div>
        </form>
    </div>

@endsection
