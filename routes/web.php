<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\PostController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [PostController::class, 'welcome'] );

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//define a route wherein a view to create a post will be returned to the user
Route::get('/posts/create', [PostController::class, 'create']);

//define a route wherein a form data will be sent via POST method to the /posts URI endpoint.
Route::post('/posts', [PostController::class, 'store']);

// define  a route that will return a view containing all the posts.
Route::get('/posts', [PostController::class, 'index']);

// define a route that will return a view containing only the authenticated users posts.
Route::get('/myPosts', [PostController::class, 'myPosts']);

// define a route wherein a view showing a specific post with the matching URL paramater ID will be returned to the user.
Route::get('/posts/{id}', [PostController::class, 'show']);

Route::get('/posts/{id}/edit', [PostController::class, 'edit']);

Route::put('/posts/{id}', [PostController::class, 'update']);

// define a route that will delete a post with the matching URL paramater ID 
//Route::delete('/posts/{id}', [PostController::class, 'destroy']);

Route::delete('/posts/{id}', [PostController::class, 'archive']);

// define a route that will allow a user to like a post
Route::put('posts/{id}/like', [PostController::class, 'like']);

Route::post('posts/{id}/comment', [PostController::class, 'comment']);


